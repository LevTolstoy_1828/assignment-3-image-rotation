#ifndef IMAGE_H
#define IMAGE_H


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>




struct image {
    size_t width, height;
    struct pixel *image;
};

struct pixel {
    uint8_t r, g, b;
};

bool image_malloc(struct image *const img, size_t width, size_t height);

void image_free(struct image *const img);

size_t image_get_size_bytes(const struct image *const img);


#endif
