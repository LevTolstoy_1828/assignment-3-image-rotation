#ifndef PADDING_H
#define PADDING_H


#include "image.h"
#include <stdio.h>



uint8_t calculate_padding(size_t image_width);

size_t padding_size_bytes(struct image const *img);

#endif
