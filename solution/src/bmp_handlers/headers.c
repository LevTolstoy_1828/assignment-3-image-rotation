#include "bmp_handlers/headers.h"
#include "image.h"
#include "padding.h"


#define LITTLE_ENDIAN_BMP 0x4D42
#define RESERVED 0
#define BIT_MAP_INFO_HEADER 40
#define BI_PLANES_BMP 1
#define BIT_COUNT 24
#define BI_RGB 0
#define X_PELS_PER_METER 2834
#define Y_PELS_PER_METER 2834
#define BI_CLR 0



struct bmp_header create_headers(const struct image * img){
    struct bmp_header header = {
            .bfType = LITTLE_ENDIAN_BMP,
            .bfileSize = sizeof(struct bmp_header) + image_get_size_bytes(img) + padding_size_bytes(img),
            .bfReserved = RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BIT_MAP_INFO_HEADER,
            .biWidth = (*img).width,
            .biHeight = (*img).height,
            .biPlanes = BI_PLANES_BMP,
            .biBitCount = BIT_COUNT,
            .biCompression = BI_RGB,
            .biSizeImage = image_get_size_bytes(img) + padding_size_bytes(img),
            .biXPelsPerMeter = X_PELS_PER_METER,
            .biYPelsPerMeter = Y_PELS_PER_METER,
            .biClrUsed = BI_CLR,
            .biClrImportant = BI_CLR
    };
    return header;
}

bool header_validation(struct bmp_header const header){
    return (header).biHeight > 0 && (header).biWidth > 0 && (header).bfType == LITTLE_ENDIAN_BMP;
}
