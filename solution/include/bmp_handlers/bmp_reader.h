#ifndef IMAGE_TRANSFORMER_BMP_READER_H
#define IMAGE_TRANSFORMER_BMP_READER_H

#include "bmp_handlers/headers.h"
#include <stdio.h>





enum read_status  {
    READ_OK = 0,
    READ_INVALID_BITS = 1,
    READ_INVALID_HEADER = 2,
    INPUT_FILE_IS_NULL = 3,
    OUT_OF_MEMORY_ERROR = 4,
    VALIDATION_ERROR = 5,
    IMAGE_ERROR = 6
};

enum read_status from_bmp(FILE *const file, struct image *const img);


#endif

