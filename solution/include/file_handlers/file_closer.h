#ifndef FILE_CLOSER_H
#define FILE_CLOSER_H

#include <stdio.h>

enum close_status {
    SUCCESS_CLOSE = 1,
    ERROR_CLOSE = 0
};

enum close_status close_file(FILE **const file);

#endif
