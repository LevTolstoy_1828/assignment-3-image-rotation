#ifndef IMAGE_TRANSFORMER_BMP_WRITER_H
#define IMAGE_TRANSFORMER_BMP_WRITER_H

#include "bmp_handlers/headers.h"
#include <stdio.h>




enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR = 1,
    OUTPUT_FILE_IS_NULL = 2
};

enum write_status to_bmp(FILE* file, struct image *img );

#endif

