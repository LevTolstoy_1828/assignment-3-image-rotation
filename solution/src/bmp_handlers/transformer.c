#include "bmp_handlers/transformer.h"
#include "image.h"


static inline void image_set_pixel(struct image *const img, struct pixel const p, size_t x, size_t y) {
    *(img->image + y * img->width + x) = p;
}
static inline struct pixel image_get_pixel(const struct image *const img, size_t x, size_t y) {
    return *(img->image + y * img->width + x);
}


struct image rotate(struct image const *img) {
    struct image rotated = {0};
    if(!image_malloc(&rotated, img->height, img->width)) return rotated;
    for(size_t i = 0; i < img->height; i++)
        for(size_t j = 0; j < img->width; j++) {
            image_set_pixel(&rotated, image_get_pixel(img, j, img->height - i - 1), i, j);
        }
    return rotated;
}
