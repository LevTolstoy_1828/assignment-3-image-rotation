#include "padding.h"

inline uint8_t calculate_padding(size_t image_width) {
    return image_width % 4;
}

inline size_t padding_size_bytes(struct image const *img) {
    return img->width * img->height * calculate_padding(img->width);
}



