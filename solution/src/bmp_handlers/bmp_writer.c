#include "bmp_handlers/bmp_writer.h"
#include "bmp_handlers/headers.h"
#include "image.h"
#include "padding.h"


enum write_status write_pixels (FILE *const file, struct image *const img) {
    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->image + i * img->width, sizeof(struct pixel), img->width, file) != img->width) return WRITE_ERROR;
        if (fseek(file, calculate_padding(img->width), SEEK_CUR) != 0) return WRITE_ERROR;
    }
    return WRITE_OK;
}



enum write_status to_bmp(FILE* file, struct image *img) {
    if (!file) return OUTPUT_FILE_IS_NULL;
    if(!img) return WRITE_ERROR;
    struct bmp_header header = create_headers(img);
    if (fwrite(&header, sizeof (struct bmp_header), 1, file) != 1)
        return WRITE_ERROR;
    return write_pixels(file, img);
}
