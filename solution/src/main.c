#include "bmp_handlers/bmp_reader.h"
#include "bmp_handlers/bmp_writer.h"
#include "bmp_handlers/transformer.h"
#include "file_handlers/file_closer.h"
#include "file_handlers/file_opener.h"
#define ERROR_CODE 1

inline static void print_error(char * error){
    fprintf(stderr, "%s \n", error);
}

int main(int argc, char **argv) {
    if (argc != 3){
        print_error("Wrong arguments!");
        return ERROR_CODE;
    }

    FILE* in_file = NULL;
    FILE* out_file = NULL;
    struct image in_image = {0};
    struct image out_image = {0};

    if (!(open_file(&in_file, argv[1], "rb") && open_file(&out_file, argv[2], "wb"))){
        print_error("File open error!");
        return 2;
    }

    if (from_bmp(in_file, &in_image)){
        print_error("File isn't BMP");
        close_file(&in_file);
        close_file(&out_file);
        return 3;
    }

    out_image = rotate(&in_image);

    if (!out_image.image)
    {
        print_error("Image rotation error!");
        close_file(&in_file);
        close_file(&out_file);
        image_free(&in_image);
        return 4;
    }

    image_free(&in_image);

    if (to_bmp(out_file, &out_image)){
        print_error("Error writing to file!");
        close_file(&in_file);
        close_file(&out_file);
        image_free(&out_image);
        return 5;
    }

    image_free(&out_image);
    close_file(&in_file);
    close_file(&out_file);
    return 0;
}
