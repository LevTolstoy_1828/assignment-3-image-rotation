#include "bmp_handlers/bmp_reader.h"
#include "bmp_handlers/headers.h"
#include "image.h"
#include "padding.h"



enum read_status read_pixels(FILE *const file, struct bmp_header *const headers, struct image *const img){
    if (!image_malloc(img, headers->biWidth, headers->biHeight))
        return OUT_OF_MEMORY_ERROR;
    for (size_t row = 0; row < img->height; row++) {
        size_t pc = fread(&(img->image[row*img->width]), sizeof(struct pixel), img->width, file);
        if (fseek(file, calculate_padding(img->width), SEEK_CUR) || pc != img->width) {
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

enum read_status from_bmp(FILE *const file, struct image *const img) {
    struct bmp_header header = {0};
    if(!img)
        return IMAGE_ERROR;
    if(!file){
        return INPUT_FILE_IS_NULL;
    }
    if (fread(&header, sizeof(struct bmp_header), 1, file) != 1)
        return READ_INVALID_HEADER;
    if (!header_validation(header))
        return VALIDATION_ERROR;
    return read_pixels(file, &header, img);
}
