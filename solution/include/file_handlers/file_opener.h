#ifndef IMAGE_TRANSFORMER_FILE_OPENER_H
#define IMAGE_TRANSFORMER_FILE_OPENER_H

#include <stdio.h>

enum open_status {
    SUCCESS_OPEN = 1,
    ERROR_OPEN = 0
};

enum open_status open_file(FILE **const file, const char *const img_filename, const char *const read_mode);

#endif


