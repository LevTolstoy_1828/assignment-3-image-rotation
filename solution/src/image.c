#include "image.h"


bool image_malloc(struct image *const img, size_t width, size_t height) {
    img->width = width;
    img->height = height;
    img->image = malloc(sizeof(struct pixel) * img->width * img->height);
    if (img->image == NULL)
        return false;
    else
        return true;
}

void image_free(struct image *const img){
    img->width = 0;
    img->height = 0;
    free(img->image);
}



size_t image_get_size_bytes(const struct image *const img) {
    return img->width * img->height * sizeof(struct pixel);
}


