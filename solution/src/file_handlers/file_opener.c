#include "file_handlers/file_opener.h"


enum open_status open_file(FILE **const file, const char *const img_filename, const char *const read_mode){
    if (!file || !img_filename || !read_mode) return ERROR_OPEN;
    *file = fopen(img_filename, read_mode);
    if (*file) return SUCCESS_OPEN;
    else return ERROR_OPEN;
}
